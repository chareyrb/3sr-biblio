import requests
import time

REQ_DOC_TYPE = "(docType_s:ART OR docType_s:OUV OR docType_s:COUV)"
BRIDGE_TYPES = {"ART": "Articles", "COMM": "Conferences", "COUV": "Books", "THESE": "These"}
SORT_BY = "producedDate_s"  # "journalDate_s"
N_PUBLI_MAX = 100
N_PUBLIS = [3, 5, 10, 15, 50, 100]
MAX_AUTHORS = 3

groups = { 'geo' : {
    'Chareyre': '0000-0001-8505-8540',
    'Viggiani': '0000-0002-2609-6077',
    'DiDonna' : '0000-0002-4383-852X',
    'Emeriault': '0000-0001-8160-1075',
    'Sibille': '0000-0002-3510-3400',
    'Villard': '0000-0002-3327-6463',
    'Combe': '0000-0002-8633-0793',
    'Tengattini': '0000-0003-0320-3340',
    'Besuelle': '0000-0001-9586-4888',
    'Richefeu': '0000-0002-8897-5499',
    'Jenck': '0000-0002-2623-1830',
    'Loret': '0000-0002-2622-3179',
    'Dano': '0000-0001-8648-7195',
    'Daudon': '0000-0003-0233-5608',
    'Desrues': '0000-0002-5514-2458',
    'Darve': '0000-0002-1276-1929',    
    }}

test=None
def html_publi_orcid(publi):
    global test
    cite = []
    test=publi
    # get names
    authors = [n['credit-name']['value'] for n in publi['contributors']['contributor']]
    if not len(authors):
        print("no authors for", publi["title"]["title"]["value"])
        return '' # if no authors, escape
    if len(authors) > MAX_AUTHORS:
        authors_displayed = authors[:MAX_AUTHORS]
        authors_displayed.append(f'<span class="author"><i>et. al.</i></span>')
    else:
        authors_displayed = authors
    cite.append(f'<span class="authors" title="">{", ".join(authors_displayed)}</span>')

    #  get title and url
    try:
        url = publi["url"]["value"]
    except:
        print('url not found')
        url = ''
    cite.append(f'<span class="article"><a href="{url}" target="_blank">{publi["title"]["title"]["value"]}</a></span>')

    # get journal
    try:
        journal = publi['journal-title']['value']
    except:
        print('journal not found')
        journal=''
    if len(journal):
        cite.append(f'<span class="journal">{journal}</span>')

    # get year
    cite.append(f'<span class="year">{publi["publication-date"]["year"]["value"]}</span>')
    
    # get month
    try:
        month = publi["publication-date"]["month"]["value"]
    except:
        month = ""

    # get doi
    try:
        doi = publi['external-ids']['external-id'][0]['external-id-value']
    except:
        doi=''
    if len(doi):
        cite.append(f'<span class="doi"><a href="https://doi.org/{doi}" >{doi}</a></span>')

    return ' '.join(cite)

def html_publi(publi):
    cite = []

    # get names and urls
    try:
        authors = []
        authors_title = []
        searchUrl = "https://hal.archives-ouvertes.fr/search/index/q/*"
        for fullname_idhal, name in zip(publi["authFullNameIdHal_fs"], publi["authLastName_s"]):
            fullname, idhal = fullname_idhal.split("_FacetSep_")
            searchType = "authIdHal_s" if idhal else "authFullName_s"
            searchString = idhal if idhal else fullname.replace(" ", "+")
            url = f"{searchUrl}/{searchType}/{searchString}"
            authors.append(f'<span class="author"><a href="{url}" target="_blank">{name.title()}</a></span>')
            authors_title.append(fullname)
    except:
        authors = publi["authFullName_s"]
        authors_title = publi["authFullName_s"]

    if len(authors) > MAX_AUTHORS:
        authors_displayed = authors[:MAX_AUTHORS]
        authors_displayed.append(f'<span class="author"><i>et. al.</i></span>')
    else:
        authors_displayed = authors
    cite.append(f'<span class="authors" title="{", ".join(publi.get("authFullName_s", []))}">[{", ".join(authors_displayed)}]</span>')

    #  get title and url
    if "uri_s" in publi and "title_s" in publi:
        cite.append(f'<span class="article"><a href="{publi["uri_s"]}" target="_blank">{publi["title_s"][0]}</a></span>')

    # get journal
    journal = []
    # for k, v in [("journalTitle_s", ""), ("volume_s", "vol."), ("page_s", "pp.")]:
    for k, v in [("journalTitle_s", "")]:
        if k in publi:
            journal.append(f'{v} {publi[k]}')
    if len(journal):
        cite.append(f'<span class="journal">{", ".join(journal)}</span>')

    # get year
    if "producedDateY_i" in publi:
        cite.append(f'<span class="year">{publi["producedDateY_i"]}</span>')

    # get doi
    if "doiId_s" in publi:
        cite.append(f'<span class="doi"><a href="https://doi.org/{publi["doiId_s"]}" >{publi["doiId_s"]}</a></span>')

    return ' '.join(cite)


def get_biblio_structure(structure_name, structure_id, n_publi=100):
    req = "http://api.archives-ouvertes.fr/search/?q=({r} AND authStructId_i:{s})&sort={so} desc&rows={n}&fl=*".format(s=structure_id, r=REQ_DOC_TYPE, n=N_PUBLI_MAX, so=SORT_BY)
    publis = requests.get(req).json()

    def write_from_publi(publis, n):
        file = "{}-{}.html".format(structure_name, n)
        print(file)
        with open(file, 'w') as f:
            f.write('<!DOCTYPE html>\n')
            f.write('<html>\n')
            f.write('\t<head>\n')
            f.write('\t\t<meta charset="UTF-8">\n')
            f.write('\t\t<link rel="stylesheet" href="biblio.css">\n')
            f.write('\t</head>\n')
            f.write('\t<body class="structure">\n')
            f.write('\t\t<div class="liste-publi">\n')
            f.write('\t\t\t<ul>\n')
            for publi in publis["response"]["docs"][:n]:
                f.write('\t\t\t\t<li>{}</li>\n'.format(html_publi(publi)))
            f.write('\t\t\t</ul>\n')
            f.write('\t\t</div>\n')
            f.write('\t</body>\n')
            f.write('</html>')
        return file

    files = []
    for n in N_PUBLIS:
        files.append(write_from_publi(publis, n))

    return files

def get_biblio_orcid(name='Chareyre',orcid='0000-0001-8505-8540',N=10,typesLists = [['journal-article'],['journal-article','conference-paper','book','book-chapter'],['all']]):
    BRIDGE_TYPES = {"ART": "journal-article", "COMM": "Conferences", "COUV": "Books", "THESE": "These"}
    req = "https://pub.orcid.org/v3.0/"+orcid+"/works"
    publis_by_type = dict()
    individual_types = dict()
    api_error = dict()
    publis = requests.get(req, headers={'Accept': 'application/json',}).json()
    publis = publis["group"]
    for i, publi in enumerate(publis):
        work=publi.get("work-summary")[0]
        type = BRIDGE_TYPES.get(work.get("type")) if BRIDGE_TYPES.get(work.get("type")) is not None else work.get("type")
        if i<N or (type in individual_types and len(individual_types[type])<N) or not type in individual_types:
            print("download",i,"th",type,"for",name)
            maxTries = 20
            while maxTries:
                try:
                    doc = requests.get('http://pub.orcid.org/'+work['path'], headers={'Accept':'application/orcid+json'}).json()
                    break
                except:
                    print('request failure ... retrying')
                    time.sleep(1)
                    maxTries-=1
                    continue
            if type in individual_types:
                if i<N: publis_by_type[type].append(doc)
                individual_types[type].append(doc)
            else:
                if i<N: publis_by_type[type] = [doc]
                individual_types[type] = [doc]    
    files = []
    for listT in typesLists:
        if listT[0]=='all':
            types = [i for i in publis_by_type.keys()]
            file = "{}_all_{}_orcid.html".format(name,N)
        else:
            types = listT
            file = "{}_{}_{}_orcid.html".format(name,"_".join(listT),N)
        publi_for_types = publis_by_type if len(listT)>1 else individual_types

        print(file)
        with open(file, 'w') as f:
            f.write('<!DOCTYPE html>\n')
            f.write('<html>\n')
            f.write('\t<head>\n')
            f.write('\t\t<meta charset="UTF-8">\n')
            f.write('\t\t<link rel="stylesheet" href="biblio.css">\n')
            f.write('\t</head>\n')
            f.write('\t<body class="perso">\n')
            for type in types:
                if not type in publi_for_types.keys(): continue
                publis = publi_for_types[type]
                f.write('\t\t<h4>{}</h4>\n'.format(type))
                f.write('\t\t<ul>\n')
                count=0
                for publi in publis:
                    htmlItem = html_publi_orcid(publi)
                    if not len(htmlItem): continue
                    f.write('\t\t\t<li>{}</li>\n'.format(htmlItem))
                    count+=1
                    if count>=N: break
                f.write('\t\t</ul>\n')
            f.write('\t</body>\n')
            f.write('</html>')
        files.append(file)

    return files


def get_biblio_group_orcid(groupName='geo', N=20, typesLists = [['Articles'],['Articles','Conference Papers and Books'],['all']]):
    global test
    BRIDGE_TYPES = {"journal-article": "Articles", "conference-paper": "Conference Papers and Books", "book-chapter": "Conference Papers and Books", "book": "Conference Papers and Books"}
    publis_by_type = dict()

    group = groups[groupName]
    for author in group:
        name = author
        orcid = group[name]
        req = "https://pub.orcid.org/v3.0/"+orcid+"/works"
        publis = requests.get(req, headers={'Accept': 'application/json',}).json()
        publis = publis["group"]
        for i, publi in enumerate(publis):
            work=publi.get("work-summary")[0]
            
            type = BRIDGE_TYPES.get(work.get("type")) if BRIDGE_TYPES.get(work.get("type")) is not None else work.get("type")
            print(author," : type",type)
            maxTries = 20
            while maxTries:
                try:
                    doc = requests.get('http://pub.orcid.org/'+work['path'], headers={'Accept':'application/orcid+json'}).json()
                    break
                except:
                    print('request failure ... retrying')
                    time.sleep(1)
                    maxTries-=1
                    continue
            
            year = doc["publication-date"]["year"]["value"]
            try: 
                key = doc['external-ids']['external-id'][0]['external-id-value']
            except:
                print("key not found")
                key =''
                
            try:
                month = publi["publication-date"]["month"]["value"]
            except:
                month = '00'
            doc['time']=year+month
            if type in publis_by_type:
                publis_by_type[type][key] = doc
            else:
                publis_by_type[type] = dict()
                publis_by_type[type][key] = doc
            if i>=10: break
    files=[]
    for listT in typesLists:
        if listT[0]=='all':
            types = [i for i in publis_by_type.keys()]
            file = "{}_all_{}_orcid.html".format(groupName,N)
        else:
            types = listT
            file = "{}_{}_{}_orcid.html".format(groupName,"_".join(listT),N)
        with open(file, 'w') as f:
            f.write('<!DOCTYPE html>\n')
            f.write('<html>\n')
            f.write('\t<head>\n')
            f.write('\t\t<meta charset="UTF-8">\n')
            f.write('\t\t<link rel="stylesheet" href="biblio.css">\n')
            f.write('\t</head>\n')
            f.write('\t<body class="structure">\n')
            for type in types:
                if not type in publis_by_type.keys(): continue
                publis = publis_by_type[type]
                test = publis.items()
                publis = {k: v for k,v in sorted(publis.items(), key=lambda item: item[1]['time'], reverse=True) }
                
                f.write('\t\t<h4>{}</h4>\n'.format(type))
                f.write('\t\t<ul>\n')
                count = 0
                for key, publi in publis.items():
                    htmlItem = html_publi_orcid(publi)
                    if not len(htmlItem): continue
                    #print(publi)
                    f.write('\t\t\t<li>{}</li>\n'.format(htmlItem))
                    count+=1
                    if count>=N: break
                f.write('\t\t</ul>\n')
            f.write('\t</body>\n')
            f.write('</html>')
            files.append(file)
    return files


def get_biblio_idhal(idhal):
    req = "http://api.archives-ouvertes.fr/search/?q=(authIdHal_s:{s})&sort={so} desc&rows={n}&fl=*".format(s=idhal, n=N_PUBLI_MAX, so=SORT_BY)
    publis_by_type = dict()
    api_error = dict()
    publis = requests.get(req).json()
    publis = publis["response"]["docs"]
    req_author = 'https://api.archives-ouvertes.fr/ref/author/?q=idHal_s:{s}'.format(s=idhal)
    idhal_info = requests.get(req_author).json()
    for i, publi in enumerate(publis):
        type = BRIDGE_TYPES.get(publi.get("docType_s")) if BRIDGE_TYPES.get(publi.get("docType_s")) is not None else publi.get("docType_s")
        if type in publis_by_type:
            publis_by_type[type].append(publi)
        else:
            publis_by_type[type] = [publi]

    file = "{}.html".format(idhal)
    print(file)
    with open(file, 'w') as f:
        f.write('<!DOCTYPE html>\n')
        f.write('<html>\n')
        f.write('\t<head>\n')
        f.write('\t\t<meta charset="UTF-8">\n')
        f.write('\t\t<link rel="stylesheet" href="biblio.css">\n')
        f.write('\t</head>\n')
        f.write('\t<body class="perso">\n')
        for type, publis in publis_by_type.items():
            f.write('\t\t<h4>{}</h4>\n'.format(type))
            f.write('\t\t<ul>\n')
            for publi in publis:
                f.write('\t\t\t<li>{}</li>\n'.format(html_publi(publi)))
            f.write('\t\t</ul>\n')
        f.write('\t</body>\n')
        f.write('</html>')

    return [file]

all_files = []
for group in groups:
    all_files.append(get_biblio_group_orcid(group))
    for author in groups[group]:
        all_files.append(get_biblio_orcid(author,groups[group][author]))

all_files.append(get_biblio_structure("geo", 1041793))
all_files.append(get_biblio_structure("comhet", 545341))
all_files.append(get_biblio_structure("rv", 545342))
all_files.append(get_biblio_structure("labo", 706))
all_files.append(get_biblio_idhal("eroubin"))

with open("index.html", "w") as f:
    print("index.html")
    f.write("<html><body><ul>")
    for files in all_files:
        for file in files:
            f.write('<li><a href="{file}">{file}</a></li>'.format(file=file))
    f.write("</ul></body></html>")
